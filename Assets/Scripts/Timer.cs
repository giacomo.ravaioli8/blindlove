﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float maxTime = 0f;
    [SerializeField]
    private float currentTime = 0f;

    //The timer starts stopped
    private bool paused = true;

    // Start is called before the first frame update
    void Start()
    {
        ResetTimer();
    }

    // Update is called once per frame
    void Update()
    {
        if (paused)
            return;

        UpdateTimer(-Time.deltaTime);
    }

    public void UpdateTimer(float variation)
    {
        currentTime += variation;

        if (currentTime <= 0f)
            GameManager.instance.Lose();

        if (currentTime >= maxTime)
            currentTime = maxTime;

        UIManager.instance.UpdateTimer(currentTime);
    }

    public void PauseUnpauseTimer(bool pause)
    {
        paused = pause;
    }

    public float GetCurrentTime()
    {
        return currentTime;
    }

    public void SetMaxTime(float time)
    {
        maxTime = time;
    }

    public void ResetTimer()
    {
        currentTime = maxTime;
    }
}
