﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    GameObject player;

    Transform mainCameraTransform;
    public Transform placeholder;

    // Start is called before the first frame update
    void Start()
    {
        mainCameraTransform = GetComponent<Transform>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.IsGameStarted() || GameManager.instance.IsTrueEnded() || GameManager.instance.IsGamePaused())
        {
            mainCameraTransform.position = new Vector3(placeholder.position.x, placeholder.position.y, mainCameraTransform.position.z);
            return;
        }

        RetrievePlayer();
        UpdatePosition();
    }

    private void RetrievePlayer()
    {
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player");
    }

    private void UpdatePosition()
    {
        if (player != null)
            mainCameraTransform.position = new Vector3(player.transform.position.x, player.transform.position.y, mainCameraTransform.position.z);
    }
}