﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputManager : MonoBehaviour
{
    public static InputManager instance = null;

    private InputHandler playerInputHandler;

    private float hInput = 0f, vInput = 0f;
    private readonly string hString = "Horizontal", vString = "Vertical";

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start() {}

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.IsGameStarted())
        {
            //if the game is: not started
            ChecksIGameNotStarted();
            return;
        }

        if (playerInputHandler == null)
        {
            ResetPlayer();
            return;
        }

        if (GameManager.instance.IsGameEnded())
        {
            if (GameManager.instance.IsGameLost())
                ChecksIGameLost();

            return;
        }

        //if the game is: started, not ended (even if is paused)
        ChecksIPauseGame();

        //if the game is: started, not ended and paused
        if (GameManager.instance.IsGamePaused())
        {
            //if the game is: started, not ended, paused and quitting
            if (GameManager.instance.IsQuitting())
            {
                ChecksIQuitting();
                return;
            }

            ChecksIGamePaused();
        }

        //Last chance input
        //if the game is: started, not ended, not paused, not quitting
        if (Input.GetKeyDown(KeyCode.P) && !GameManager.instance.IsTutorial())
            GameManager.instance.LastChanceReset();

        //Check movement input
        ChecksIMovement();
        //Check light related input
        ChecksILight();
    }

    private void ChecksIGameNotStarted()
    {
        if (GameManager.instance.IsQuitting())
        {
            //if the game is: not started, quitting
            ChecksIQuitting();
        }
        else
        {
            //Start the game
            if (Input.GetKeyDown(KeyCode.Space))
                GameManager.instance.StartGame();

            //Prompt quit
            if (Input.GetKeyDown(KeyCode.Q))
            {
                Debug.Log("TODO: prompt \"are you sure?\" quit");
                GameManager.instance.QuitUnquit();
            }
        }
    }

    private void ChecksIMovement()
    {
        hInput = Input.GetAxis(hString);
        vInput = Input.GetAxis(vString);

        playerInputHandler.Move(new Vector3(hInput, vInput));
    }

    private void ChecksILight()
    {
        /* 1 - Timer
         * 2 - Goal
         * 3 - Inner Walls
         * 4 - Ground Holes (Floor)
         * 5 - Sentinels (Moving Obstacles)
         * 6 - Rewards
         * R - Reset
         */
        if (Input.GetKeyDown(KeyCode.Alpha1))
            playerInputHandler.TurnOnLight(-1, true);

        if (Input.GetKeyDown(KeyCode.Alpha2))
            playerInputHandler.TurnOnLight((int)LightController.LightInfo.Goal, true);

        if (Input.GetKeyDown(KeyCode.Alpha3))
            playerInputHandler.TurnOnLight((int)LightController.LightInfo.InnerWall, true);

        if (Input.GetKeyDown(KeyCode.Alpha4))
            playerInputHandler.TurnOnLight((int)LightController.LightInfo.GroundHole, true);

        if (Input.GetKeyDown(KeyCode.Alpha5))
            playerInputHandler.TurnOnLight((int)LightController.LightInfo.Bomb, true);

        if (Input.GetKeyDown(KeyCode.Alpha6))
            playerInputHandler.TurnOnLight((int)LightController.LightInfo.Reward, true);

        if (Input.GetKeyDown(KeyCode.R))
            playerInputHandler.TurnOnLight((int)LightController.LightInfo.NoLight);
    }

    private void ChecksIQuitting()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (!GameManager.instance.IsTutorial())
                GameManager.instance.HardReset();
            else
                GameManager.instance.TutorialHardReset();

            SceneManager.LoadScene(0);
        }   

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.instance.QuitUnquit();
        }
    }

    private void ChecksIGamePaused()
    {
        //only q because back is checked whenever the game is started, not ended
        if (Input.GetKeyDown(KeyCode.Q))
        {
            GameManager.instance.QuitUnquit();
        }
    }

    private void ChecksIPauseGame()
    {
        //Check for the PAUSE/UNPAUSE input
        if (Input.GetKeyDown(KeyCode.Escape) && !GameManager.instance.IsQuitting())
            GameManager.instance.PauseUnpauseGame(!GameManager.instance.IsGamePaused());
    }

    private void ChecksIGameLost()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameManager.instance.HardReset();
            GameManager.instance.PrepareToStart();
        }
        
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.instance.HardReset();
            SceneManager.LoadScene(0);
        }
    }

    public void ResetPlayer()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        if(player != null)
            playerInputHandler = player.GetComponent<InputHandler>();
        
    }

}