﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance = null;

    [Header("Tutorial UI")]
    public Text tutorialBoom;
    public Text tutorialEnd;

    [Header("Game UI")]
    public List<int> startScreenE = new List<int>();
    public List<int> playScreenE = new List<int>();
    public List<int> pauseScreenE = new List<int>();
    public List<int> quitScreenE = new List<int>();
    public List<int> winScreenE = new List<int>();
    public List<int> lossScreenE = new List<int>();

    public List<GameObject> hotkeys = new List<GameObject>();
    private int hotkeysHash;

    public Text timer;
    public Text score;
    public Text winRound;
    public Text lostRound;

    public Image heartStamina;

    public List<GameObject> gameUIElementsList = new List<GameObject>();
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start() {

        ShowStartUI();
        hotkeysHash = Animator.StringToHash("Selected");

    }

    // Update is called once per frame
    void Update() {}

    public void ShowStartUI()
    {
        EnableAtIndex(startScreenE);
    }

    public void ShowPlayUI()
    {
        EnableAtIndex(playScreenE);
    }

    public void ShowPauseUI()
    {
        EnableAtIndex(pauseScreenE);
    }

    public void ShowQuitUI()
    {
        EnableAtIndex(quitScreenE);
    }

    public void ShowWinUI()
    {
        EnableAtIndex(winScreenE);
    }

    public void ShowLossUI()
    {
        EnableAtIndex(lossScreenE);
    }
    
    public void RemoveAllUI()
    {
        EnableAtIndex(new List<int> { -1 });
    }


    public void UpdateTimer(float value)
    {
        int minutes = Mathf.FloorToInt(value / 60f);
        int seconds = Mathf.FloorToInt(value % 60f);

        timer.text = minutes + ":" + (seconds < 10 ? ("0" + seconds) : ""+seconds);
    }

    public void UpdateScore(float value)
    {
        score.text = "Love points\n" + value.ToString();
    }

    public void ShowTimer (bool visible)
    {
        timer.enabled = visible;
    }

    public void UpdateNRoundPanel(int nRound)
    {
        winRound.text = nRound.ToString();
        lostRound.text = nRound.ToString();
    }

    public void HighlightHotkey(int idxFrom, int idxTo)
    {
        if (idxFrom == idxTo)
            return;
        
        if(idxFrom == hotkeys.Count)
        {
            hotkeys[idxTo].GetComponent<Animator>().SetBool(hotkeysHash, true);
            return;
        }

        if(idxTo == hotkeys.Count)
        {
            hotkeys[idxFrom].GetComponent<Animator>().SetBool(hotkeysHash, false);
            return;
        }

        if (idxFrom != idxTo)
        {
            hotkeys[idxFrom].GetComponent<Animator>().SetBool(hotkeysHash, false);
            hotkeys[idxTo].GetComponent<Animator>().SetBool(hotkeysHash, true);
        }
    }
    
    public void ShowBoomTutorial(bool visible)
    {
        tutorialBoom.enabled = visible;
    }

    public void ShowTutorialEnd(bool visible)
    {
        tutorialEnd.enabled = visible;
    }

    public void UpdateHeartStamina(float scale, Color color)
    {
        heartStamina.transform.localScale = new Vector3(scale, scale, 1f);
        heartStamina.color = color;
    }

    private bool FindInList(List<int> list, int idx)
    {
        return list.Contains(idx);
    }

    private void EnableAtIndex(List<int> indexToActivate)
    {
        for (int i = 0; i < gameUIElementsList.Count; i++)
            gameUIElementsList[i].SetActive(FindInList(indexToActivate, i));
    }

}
