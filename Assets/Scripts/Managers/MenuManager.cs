﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{

    public GameObject menuUIElements;
    public GameObject infoUIElements;
    /*
     * 0 - Menu
     * 1 - Info
     */
    public short activeScreen = -1;

    public Text highscore;

    // Start is called before the first frame update
    void Start()
    {
        activeScreen = 0;
        LoadHighscore();
    }

    // Update is called once per frame
    void Update()
    {
        CheckAboutPanel(); //Escape button

        //Start the game
        if (Input.GetKeyDown(KeyCode.Space))
            SceneManager.LoadScene(1);

        //Start the tutorial
        if (Input.GetKeyDown(KeyCode.T))
        {
            GameManager.instance.SetIsTutorial();
            SceneManager.LoadScene(4);
        }

        //Delete the highscore
        if (Input.GetKeyDown(KeyCode.K))
        {
            SavingSystem.Delete();
            highscore.text = "Best run:\n Round 0 \n 0 Points";
        }
        
        //Quit the game
        if (Input.GetKeyDown(KeyCode.Q))
            Application.Quit();
    }

    private void CheckAboutPanel()
    {
        //Switch from title screen to about screen and vice-versa
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            switch (activeScreen)
            {
                case 0:
                    menuUIElements.SetActive(false);
                    activeScreen = 1;
                    infoUIElements.SetActive(true);
                    break;
                case 1:
                    menuUIElements.SetActive(true);
                    activeScreen = 0;
                    infoUIElements.SetActive(false);
                    break;
            }
        }
    }

    private void LoadHighscore()
    {
        PlayerData playerData = SavingSystem.Load();

        if (playerData != null)
        {
            highscore.text = "Best run:\n Round " + playerData.highestRound + "\n" + playerData.highestScore + " Points";
        }
    }
}
