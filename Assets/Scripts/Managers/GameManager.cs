﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Experimental.Rendering.LWRP;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    [HideInInspector]
    public Timer timer;
    [HideInInspector]
    public Light2D globalLight;

    [Header("Tutorial")]
    public bool isTutorial = false;
    public bool tutorialEnded = false;
    public float tutorialWonDelay = 0f;

    [Header("Game")]
    public float wonDelay;
    public float lostDelay;
    
    [Space(15f)]
    public List<LevelChallengeSettings> challengeLevelsSettings = new List<LevelChallengeSettings>();

    [Header("Public variables for debug")]
    [SerializeField]
    private bool started = false; //true -> game is started
    [SerializeField]
    private bool paused = false; //true -> game is paused
    [SerializeField]
    private bool lost = false; //true -> game lost
    [SerializeField]
    private bool won = false; //true -> game won
    [SerializeField]
    private bool ended = false; //true -> game ended
    [SerializeField]
    private bool trueEnded = false;
    [SerializeField]
    private bool quitting = false; //true -> game ended

    private GameObject player;

    [Header("Game Progression")]
    public int levelsPerChallengeStep = 3;
    [SerializeField]
    private int nRound = 1;
    [SerializeField]
    private int score = 0;
    private int lastScore = 0;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        timer = GetComponent<Timer>();
        globalLight = GetComponent<Light2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //If the game is ended a.k.a. winScreen and lostScreen or not started a.k.a. waitingScreen and mainMenu or paused
        if (!started || paused || ended)
            return;

        if (player == null)
        {
            player = SetPlayer();
        }
            

        if (isTutorial && !tutorialEnded)
            return;
    }
    
    private GameObject SetPlayer()
    {
        return GameObject.FindGameObjectWithTag("Player");
    }

    private void LoadLevelSettings()
    {
        
        if (isTutorial)
        {
            timer.SetMaxTime(challengeLevelsSettings[9].timeAvailable);
            SpawnManager.instance.SetLevelChallenge(challengeLevelsSettings[9]);

            SpawnManager.instance.ComputeGrid();
            return;
        }

        if (nRound <= 9)
        {
            timer.SetMaxTime(challengeLevelsSettings[nRound - 1].timeAvailable);
            SpawnManager.instance.SetLevelChallenge(challengeLevelsSettings[nRound - 1]);

            if (nRound % levelsPerChallengeStep == 1)
                SpawnManager.instance.ComputeGrid();
        }
        else
        {
            timer.SetMaxTime(challengeLevelsSettings[8].timeAvailable);
            SpawnManager.instance.SetLevelChallenge(challengeLevelsSettings[8]);
        }

        timer.ResetTimer();
    }

    public void PrepareToStart()
    {

        RestoreBools();
        
        UIManager.instance.ShowStartUI();
    }

    //Create the level and start the game
    public void StartGame()
    {
        Debug.Log("------ Starting the game ------");
        started = true;

        //if the first half of the tutorial is started
        if (isTutorial && !tutorialEnded)
        {
            UIManager.instance.ShowPlayUI();
            GameObject.FindGameObjectWithTag("Player").GetComponent<LightController>().TurnOnLightInfo(0, true);
            return;
        }

        //Apply the level settings based on the current round, create the structure of the level
        LoadLevelSettings();

        SpawnManager.instance.CreateLevel();

        UIManager.instance.ShowPlayUI();
        timer.PauseUnpauseTimer(false);
    }

    public void TutorialHardReset()
    {
        RestoreBools();

        isTutorial = false;
        tutorialEnded = false;

        score = 0;
        nRound = 1;
    }
    
    public void HardReset()
    {
        RestoreBools();
        
        score = 0;
        nRound = 1;
    }

    //Restore the level to recreate it from a new time
    public void LastChanceReset()
    {
        score = lastScore;

        PrepareToStart();
    }

    //Handle the increment of the challenge
    public void NextLevel()
    {
        UIManager.instance.UpdateNRoundPanel(nRound);

        nRound++;
        lastScore = score;

        Debug.Log("Round " + nRound);

        if (nRound == 4)
            SceneManager.LoadScene(2);
        else if (nRound == 7)
            SceneManager.LoadScene(3);
        
        PrepareToStart();
    }

    //Pause the game
    public void PauseUnpauseGame(bool pause)
    {
        paused = pause;
        timer.PauseUnpauseTimer(pause);

        if (paused)
        {
            Debug.Log("------ Pausing the game ------");
            UIManager.instance.ShowPauseUI();
        }
        else
        {
            Debug.Log("------ Unpausing the game ------");
            UIManager.instance.ShowPlayUI();
        }
            
    }

    //Open quit confirmation
    public void QuitUnquit()
    {
        quitting = !quitting;

        if (quitting)
        {
            Debug.Log("------ Quitting the game ------");
            UIManager.instance.ShowQuitUI();
        }
        else if (!started)
        {
            Debug.Log("------ Back to start ------");
            UIManager.instance.ShowStartUI();
        }   
        else
        {
            Debug.Log("------ Back to pause ------");
            UIManager.instance.ShowPauseUI();
        }   
    }

    public void Lose()
    {
        Debug.Log("------ Lost the game ------");
        if (ended)
            return;

        lost = true;
        ended = true;
        timer.PauseUnpauseTimer(true);

        player.GetComponent<AnimationController>().deathEffect.Play();
        player.GetComponentInChildren<SpriteRenderer>().enabled = false;
        player.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        player.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        StartCoroutine(ILostCoroutine(lostDelay));
    }

    public void Win()
    {
        Debug.Log("------ Won the game ------");
        if (ended)
            return;

        won = true;
        ended = true;
        timer.PauseUnpauseTimer(true);

        IncrementScore(500 * nRound);

        player.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        player.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;

        if (!isTutorial)
        {
            UIManager.instance.ShowWinUI();
            StartCoroutine(IWonCoroutine(wonDelay));
        }   
        else
        {
            StartCoroutine(IWonCoroutine(tutorialWonDelay));
        }
    }

    public void IncrementScore(int value)
    {
        score += value;
        UIManager.instance.UpdateScore(score);
    }

    public void TurnOnLight(float time)
    {
        StartCoroutine(ITurnOnGlobalLightForSeconds(time));
    }
    
    public void SetIsTutorial()
    {
        isTutorial = true;
    }

    public bool IsTutorial()
    {
        return isTutorial;
    }

    public bool IsTutorialEnded()
    {
        return tutorialEnded;
    }

    public void StartTutorialExample()
    {
        LastChanceReset();
    }
    
    public void SetTrueEnding(bool ended)
    {
        trueEnded = ended;
    }

    public bool IsGameStarted()
    {
        return started;
    }

    public bool IsGamePaused()
    {
        return paused;
    }

    public bool IsGameLost()
    {
        return lost;
    }

    public bool IsGameWon()
    {
        return won;
    }

    public bool IsGameEnded()
    {
        return ended;
    }

    public bool IsTrueEnded()
    {
        return trueEnded;
    }

    public bool IsQuitting()
    {
        return quitting;
    }

    public int GetRoundNumber()
    {
        return nRound;
    }

    public int GetScore()
    {
        return score;
    }

    public void SaveHishscore()
    {
        bool save = false;
        PlayerData playerData = SavingSystem.Load();

        if(playerData != null)
        {
            if (nRound > playerData.highestRound)
            {
                playerData.highestRound = nRound;
                save = true;
            }

            if (score > playerData.highestScore)
            {
                playerData.highestScore = score;
                save = true;
            }
        }
        else
        {
            playerData = new PlayerData();
            save = true;
        }

        if(save)
            SavingSystem.Save(playerData);
    }

    private void RestoreBools()
    {
        started = false;
        paused = false;
        won = false;
        lost = false;
        ended = false;
        trueEnded = false;
        quitting = false;
    }

    private IEnumerator IWonCoroutine(float delay)
    {
        yield return new WaitForSeconds(delay);
        
        if (isTutorial && !tutorialEnded)  //first half of the tutorial has just ended
        {
            tutorialEnded = true;
            StartTutorialExample();
        }
        else if (isTutorial && tutorialEnded) //second half of the tutorial has just ended
        {
            TutorialHardReset();
            SceneManager.LoadScene(0);
        }
        else if (!isTutorial)   //normal level
        {
            NextLevel();
        }
    }

    private IEnumerator ILostCoroutine(float delay)
    {
        yield return new WaitForSeconds(delay);

        trueEnded = true;

        UIManager.instance.ShowLossUI();

        if (nRound > 1)
            SaveHishscore();
    }

    private IEnumerator ITurnOnGlobalLightForSeconds(float time)
    {
        globalLight.enabled = true;

        yield return new WaitForSeconds(time);

        globalLight.enabled = false;
    }
}
