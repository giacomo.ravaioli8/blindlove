﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager instance = null;
    
    [Header("Player Spawn")]
    public List<Transform> spawnTransforms = new List<Transform>();
    public GameObject playerPrefab;
    public GameObject goalPrefab;
    private short actualPlayerSpawnIdx = 0;
    private short actualGoalSpawnIdx = 0;
    public float playerSafeZone = 4f;

    [Header("Inner Walls Spawn")]
    public GameObject innerWallsContainer;
    public GameObject innerWallPrefab;
    public float spikeInnerWallChance = 0.15f;
    public GameObject innerSpikeWallPrefab;
    public GameObject innerSpikeWallsContainer;

    [Header("Ground Holes Spawn")]
    public int nGroundHoles = 14;
    public float groundHolesVar = 0f;
    public GameObject groundHolesContainer;
    public GameObject groundHolePrefab; 

    [Header("Bombs Spawn")]
    public int nBombs = 12;
    public int nDoubleThreat = 3;
    public float bombsVar = 0f;
    public GameObject bombsContainer;
    public GameObject bombPrefab;

    [Header("Rewards Spawn")]
    public int nRewards = 4;
    public float rewardsVar = 0f;
    public GameObject rewardsContainer;
    public GameObject rewardPrefab;

    [Header("PUPs Spawn")]
    public int nTimePup = 1;
    public int nLightPup = 1;
    public float pUPVar = 0f;
    public GameObject pUPSContainer;
    public GameObject timePUPPrefab;
    public GameObject lightPUPPrefab;
    
    public float nodesLowerBound;
    public float nodesUpperBound;

    public float middleLowerBound;
    public float middleUpperBound;

    public float step = 3.5f;
    private float nGridCells = 4;

    private List<Vector3> gridMiddles = new List<Vector3>();
    private List<Vector3> gridNodes = new List<Vector3>();
    private List<short> positions = new List<short>();
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Random.InitState(System.DateTime.Now.Millisecond);
    }

    // Update is called once per frame
    void Update() {}

    public void SetLevelChallenge(LevelChallengeSettings challengeLevel)
    {
        //Grid
        nodesLowerBound = challengeLevel.nodesLowerBound;
        nodesUpperBound = challengeLevel.nodesUpperBound;

        middleLowerBound = challengeLevel.middleLowerBound;
        middleUpperBound = challengeLevel.middleUpperBound;

        //Items and Game objects
        nGroundHoles = challengeLevel.nGroundHoles;
        groundHolesVar = challengeLevel.groundHolesVar;

        spikeInnerWallChance = challengeLevel.spikeInnerWallChance;

        nBombs = challengeLevel.nBombs;
        nDoubleThreat = challengeLevel.nDoubleThreat;
        bombsVar = challengeLevel.bombsVar;

        nRewards = challengeLevel.nRewards;
        rewardsVar = challengeLevel.rewardsVar;

        nTimePup = challengeLevel.timePup;
        nLightPup = challengeLevel.lightPup;
        pUPVar = challengeLevel.pUPVar;
    }

    public void ComputeGrid()
    {
        nGridCells = (nodesUpperBound - nodesLowerBound) / step;
        Debug.Log("Grid cells " + nGridCells);

        Debug.Log("Node Upper Bound " + nodesUpperBound);
        Debug.Log("Node Lower Bound " + nodesLowerBound);

        CreateGrid();
    }

    public void CreateLevel()
    {
        CreateIdxVector();

        ClearAll();

        SpawnPlayer();
        ClearZoneAroundPlayerGoal();
        SpawnInnerWalls();
        SpawnGroundHoles();
        SpawnBombs();
        SpawnRewards();
        SpawnPUP();
    }

    private void ClearAll()
    {
        if (GameObject.FindGameObjectWithTag("Player") != null)
            Destroy(GameObject.FindGameObjectWithTag("Player"));

        if (GameObject.FindGameObjectWithTag("Goal") != null)
            Destroy(GameObject.FindGameObjectWithTag("Goal"));

        GameObject[] groundHoles = GameObject.FindGameObjectsWithTag("GroundHole");
        foreach(GameObject obj in groundHoles)
            Destroy(obj);

        GameObject[] bombs = GameObject.FindGameObjectsWithTag("Bomb");
        foreach (GameObject obj in bombs)
            Destroy(obj);

        GameObject[] rewards = GameObject.FindGameObjectsWithTag("Reward");
        foreach (GameObject obj in rewards)
            Destroy(obj);

        GameObject[] pUps = GameObject.FindGameObjectsWithTag("pUP");
        foreach (GameObject obj in pUps)
            Destroy(obj);

        GameObject[] pastInnerWalls = GameObject.FindGameObjectsWithTag("InnerWall");
        foreach (GameObject obj in pastInnerWalls)
            Destroy(obj);

        GameObject[] pastSpikeInnerWalls = GameObject.FindGameObjectsWithTag("SpikeInnerWall");
        foreach (GameObject obj in pastSpikeInnerWalls)
            Destroy(obj);
    }

    private void SpawnPlayer()
    {
        actualPlayerSpawnIdx = (short) Random.Range(0, spawnTransforms.Count - 1);
        actualGoalSpawnIdx = (short) ((actualPlayerSpawnIdx + 2) % 4);

        Instantiate(playerPrefab, spawnTransforms[actualPlayerSpawnIdx]);
        Instantiate(goalPrefab, spawnTransforms[actualGoalSpawnIdx]);
        
        Debug.Log("Player spawn = " + actualPlayerSpawnIdx);
        Debug.Log("Goal spawn = " + actualGoalSpawnIdx);
    }

    private void SpawnInnerWalls()
    {
 
        int rand = Random.Range(0, 100);
        int i;

        bool spike = false;
        float spawnAngle = 0f;

        for (i = rand < 50 ? 1 : 0; i < gridNodes.Count; i++)
        {
            spawnAngle = Random.Range(0, 3) * 90f;
            spike = Random.Range(0, 100) < (spikeInnerWallChance * 100);

            if(spike)
                Instantiate(innerSpikeWallPrefab, gridNodes[i], Quaternion.Euler(0f, 0f, spawnAngle), innerSpikeWallsContainer.transform);
            else
                Instantiate(innerWallPrefab, gridNodes[i], Quaternion.Euler(0f, 0f, spawnAngle), innerWallsContainer.transform);
        }

    }

    private void SpawnGroundHoles()
    {
        short[] pos = GetNAvailablePositions(nGroundHoles);
        SpawnObjectInCircle(groundHolePrefab, pos, groundHolesContainer, groundHolesVar);
    }

    private void SpawnBombs()
    {
        short[] pos = GetNAvailablePositions(nBombs);

        for (int i=(nBombs - nDoubleThreat); i < pos.Length; i++)
        {
            pos[i] = pos[i - nBombs + nDoubleThreat];
        }

        SpawnObjectInCircle(bombPrefab, pos, bombsContainer, bombsVar);
    }

    private void SpawnRewards()
    {
        short[] pos = GetNAvailablePositions(nRewards);
        SpawnObjectInCircle(rewardPrefab, pos, rewardsContainer, rewardsVar);
    }

    private void SpawnPUP()
    {
        short[] pos = GetNAvailablePositions(nLightPup);
        SpawnObjectInCircle(lightPUPPrefab, pos, pUPSContainer, pUPVar);

        short[] pos2 = GetNAvailablePositions(nTimePup);
        SpawnObjectInCircle(timePUPPrefab, pos2, pUPSContainer, pUPVar);
    }

    private void ClearZoneAroundPlayerGoal()
    {
        List<short> temp = new List<short>();

        foreach (short s in positions)
        {
            if (Vector3.Distance(spawnTransforms[actualGoalSpawnIdx].position, gridMiddles[s]) < playerSafeZone || Vector3.Distance(spawnTransforms[actualPlayerSpawnIdx].position, gridMiddles[s]) < playerSafeZone)
                temp.Add(s);
        }

        foreach (short s in temp)
        {
            positions.Remove(s);
        }
    }

    private void SpawnObjectInCircle(GameObject objToSpawn, short[] pos, GameObject parent, float variance)
    {
        for (int i = 0; i < pos.Length; i++)
        {
            Vector2 rand = Random.insideUnitCircle * variance;
            Instantiate(objToSpawn, gridMiddles[pos[i]] + new Vector3(rand.x, rand.y), Quaternion.identity, parent.transform);
        }
    }

    private short[] GetNAvailablePositions(int n)
    {
        int rand; int i;
        short[] pos = new short[n];
        
        for (i=0; i < n; i++)
        {
            rand = Random.Range(0, positions.Count - 1);

            pos[i] = positions[rand];
            positions.Remove(positions[rand]);
        }
        
        return pos;
    }
    
    private void CreateIdxVector()
    {
        positions.Clear();

        Debug.Log("GridMiddle Count " + gridMiddles.Count);

        for (short i = 0; i < gridMiddles.Count; i++)
        {
            positions.Add(i);
        }

        Debug.Log("POS " + positions.Count);
    }

    private void CreateGrid()
    {
        //Nodes in the grid
        int i, j;
        float x = nodesLowerBound, y = nodesUpperBound;
        float xMiddle = middleLowerBound, yMiddle = middleUpperBound;

        gridNodes.Clear();
        gridMiddles.Clear();

        for (i = 0; i < nGridCells - 1; i++)
        {
            y -= step;

            for (j = 0; j < nGridCells - 1; j++)
            {
                x += step;
                gridNodes.Add(new Vector3(x, y, 0f));
            }

            x = nodesLowerBound;
        }

        Debug.Log("Grid nodes = " + gridNodes.Count);

        //For middle points in the grid
        for (i = 0; i < nGridCells; i++)
        {
            yMiddle -=step;

            for (j = 0; j < nGridCells; j++)
            {
                xMiddle += step;
                gridMiddles.Add(new Vector3(xMiddle, yMiddle, 0f));
            }
            
            xMiddle = middleLowerBound;
        }

        Debug.Log("Grid middles = " + gridMiddles.Count);
    }
}
