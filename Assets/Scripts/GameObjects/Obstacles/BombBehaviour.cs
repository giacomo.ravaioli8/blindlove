﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

public class BombBehaviour : MonoBehaviour
{

    public CircleCollider2D alarmTrigger;
    public Light2D bombLight;
    public float rotationSpeed;

    public float explosionTime = 1f;
    
    private bool alarmActive = false;
    private float timer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        timer = explosionTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.IsGameEnded())
            return;

        if (alarmActive)
        {
            timer -= Time.deltaTime;

            if (timer <= 0.01f)
            {
                if (GameManager.instance.isTutorial && !GameManager.instance.IsTutorialEnded())
                {
                    UIManager.instance.ShowBoomTutorial(true);
                    timer = explosionTime;
                    alarmActive = false;
                    return;
                }

                if (alarmActive && !GameManager.instance.IsGameEnded())
                    GameManager.instance.Lose();

                //EXPLOSION EFFECT
                Destroy(gameObject);              
            }

            transform.Rotate(Vector3.forward, rotationSpeed * (1 / timer + 0.2f) * Time.deltaTime);
        }
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            alarmActive = true;
            bombLight.enabled = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            alarmActive = false;
            bombLight.enabled = false;
            timer = explosionTime;
        }
    }
}
