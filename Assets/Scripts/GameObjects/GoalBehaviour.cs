﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoalBehaviour : MonoBehaviour
{
    public Text prompt;
    public Transform destination;
    public Animator anim;
    public float speed = 6f;

    private bool moved = false;
    private bool moveGoal = false;
    private Rigidbody2D rb;

    public Collider2D coll;

    // Start is called before the first frame update
    void Start() {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        if (!GameManager.instance.isTutorial)
            return;

        if (moveGoal)
            rb.AddForce(new Vector2(1f * speed * Time.deltaTime, 0f));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!GameManager.instance.IsTutorial())
            return;

        if (collision.gameObject.tag.Equals("Player"))
        {
            //the goal starts moving towards its destination
            if (!moved) {
                prompt.enabled = true;
                moveGoal = true;
                coll.enabled = false;
                anim.SetBool("Moving", true);
            }
            else
            {
                //the first half of the tutorial ends
                rb.bodyType = RigidbodyType2D.Static;
                StartCoroutine(IEndTutorial());
            }
        }

        if (collision.gameObject.tag.Equals("Trigger"))
        {
            moveGoal = false;
            coll.enabled = true;
            moved = true;
            rb.velocity = Vector3.zero;
            transform.position = destination.position;
            anim.SetBool("Moving", false);
            Destroy(collision.gameObject);
        }
    }

    private IEnumerator IEndTutorial()
    {
        UIManager.instance.ShowTutorialEnd(true);
        UIManager.instance.RemoveAllUI();
        GameObject.FindGameObjectWithTag("Player").GetComponent<LightController>().TurnOnLightInfo(0, true);

        yield return new WaitForSeconds(3);
        
        GameManager.instance.Win();
    }
}
