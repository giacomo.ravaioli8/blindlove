﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimePUP : MonoBehaviour
{

    public float addedTime = 0f;
    public float rotationSpeed = 180f;

    // Start is called before the first frame update
    void Start() {}

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward, rotationSpeed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            GameManager.instance.timer.UpdateTimer(addedTime);
            Destroy(gameObject);
        }
    }
}
