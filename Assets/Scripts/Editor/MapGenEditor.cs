﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.EditorTools;


[CustomEditor(typeof(SpawnManager))]
public class MapGenEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        SpawnManager sMan = (SpawnManager) target;

        if (GUILayout.Button("Create Level"))
            sMan.CreateLevel();
    }
}
