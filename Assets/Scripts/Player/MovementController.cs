﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public Vector3 Destination { set; get; }

    private Rigidbody2D rb;

    //Controllers
    private AnimationController animController;

    public int speed = 1;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animController = GetComponent<AnimationController>();
    }

    // Update is called once per frame
    void Update() {}

    private void FixedUpdate()
    {
        if (Destination.magnitude > 0f)
        {
            rb.MovePosition(transform.position + Destination * speed * Time.fixedDeltaTime);
            animController.Move(true);
        }
        else
        {
            animController.Move(false);
        }
    }

    private void UpdateRotation(float angle)
    {
        rb.MoveRotation(angle);
    }
}
