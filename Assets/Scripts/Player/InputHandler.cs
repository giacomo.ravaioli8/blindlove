﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    
    private MovementController movementController;
    private LightController lightController;
    private AnimationController animController;

    // Start is called before the first frame update
    void Start()
    {
        movementController = GetComponent<MovementController>();
        lightController = GetComponent<LightController>();
        animController = GetComponent<AnimationController>();
    }

    // Update is called once per frame
    void Update() {}

    public void Move(Vector3 destination)
    {
        movementController.Destination = destination;
    }

    public void TurnOnLight (int light, bool value = false)
    {
        animController.swtichEffect.Play();
        lightController.TurnOnLightInfo(light, value);
    }
}
