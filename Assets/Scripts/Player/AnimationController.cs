﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    public Animator shapeAnim;
    public Animator lightAnim;

    public ParticleSystem deathEffect;
    public ParticleSystem swtichEffect;

    private int movingIdx;
    private int startIdx;
    private bool startAnimFired = false;

    // Start is called before the first frame update
    void Start()
    {
        GetParameterIdx();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.IsGameStarted())
        {
            if (startAnimFired)
            {
                ChangeLight(false);
                startAnimFired = false;
            }
            return;
        }

        if (!startAnimFired)
        {
            ChangeLight(true);
            startAnimFired = true;
        }
    }

    private void GetParameterIdx()
    {
        //Moving parameter
        movingIdx = Animator.StringToHash("Moving");

        //LightUp parameter
        startIdx = Animator.StringToHash("LightUp");
    }

    public void Move(bool value)
    {
        shapeAnim.SetBool(movingIdx, value);
    }

    public void ChangeLight(bool value)
    {
        lightAnim.SetBool(startIdx, value);
    }
}
