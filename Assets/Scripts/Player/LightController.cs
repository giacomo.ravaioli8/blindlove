﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

public class LightController : MonoBehaviour
{
    public enum LightInfo : short { Goal, InnerWall, GroundHole, Bomb, Reward, NoLight }

    [Header("Light")]
    public Light2D blindLight;
    public int activeLight = 5;
    public List<Light2D> infoLights = new List<Light2D>();

    [Header("Stamina")]
    public float staminaMax = 0f;
    public float staminaDropRate = 0f;
    public float staminaRaiseRate = 0f;
    public float stamina = 0f;

    [Header("Visual stamina hint")]
    public float fullStaminaScale = 1f;
    public float emptyStaminaScale = 0.1f;

    public Color fullStaminaColor = Color.white;
    public Color emptyStaminaColor = Color.red;

    // Start is called before the first frame update
    void Start()
    {
        stamina = staminaMax;
        
        if (blindLight == null)
            blindLight = GetComponent<Light2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.IsGameStarted() || GameManager.instance.IsGamePaused() || GameManager.instance.IsGameEnded())
            return;

        if (GameManager.instance.IsTutorial() && !GameManager.instance.IsTutorialEnded())
            return;

        if (activeLight == infoLights.Count)
        {
            stamina += Time.deltaTime * staminaRaiseRate;

            if (stamina > staminaMax)
                stamina = staminaMax;
        }
        else
        {
            stamina -= Time.deltaTime * staminaDropRate;
        }

        if (stamina <= 0)
            GameManager.instance.Lose();

        UpdateStaminaUI();
    }

    /*
     * If value is false, then turn off all the lights.
     * Otherwise turn on the specified light.
     */
    public void TurnOnLightInfo (int lightInfo, bool value = false)
    {

        int pastLight = activeLight;

        if (!value)
        {
            if (activeLight == -1)
                UIManager.instance.ShowTimer(false);
            else
                infoLights[activeLight].enabled = false;

            activeLight = infoLights.Count;
        }
        else
        {
            if (activeLight == -1)
                UIManager.instance.ShowTimer(false);
            else if (activeLight != infoLights.Count)
                infoLights[activeLight].enabled = false;

            if (lightInfo == -1)
                UIManager.instance.ShowTimer(true);
            else
                infoLights[lightInfo].enabled = true;

            activeLight = lightInfo;
        }

        UIManager.instance.HighlightHotkey(pastLight + 1, activeLight + 1);
        
    }

    /*
     * Value indicate the remaining stamina
     */
    private void UpdateStaminaUI()
    {
        float staminaPercentage = Mathf.Clamp01(stamina / staminaMax);
        float scale = (staminaPercentage * fullStaminaScale) + ((1 - staminaPercentage) * emptyStaminaScale);
        Color color = (staminaPercentage * fullStaminaColor) + ((1 - staminaPercentage) * emptyStaminaColor);

        UIManager.instance.UpdateHeartStamina(scale, color);
    }

}
