﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionController : MonoBehaviour
{
    private Collider2D coll;
    private AnimationController animController;

    public string goalLayerName;
    public string stillObstacleLayerName;
    public string spikeWallLayerName;

    // Start is called before the first frame update
    void Start()
    {
        coll = GetComponent<Collider2D>();
        animController = GetComponent<AnimationController>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (GameManager.instance.IsTutorial() && !GameManager.instance.IsTutorialEnded())
            return;

        if (collision.gameObject.layer == LayerMask.NameToLayer(goalLayerName))
        {
            GameManager.instance.Win();
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer(stillObstacleLayerName))
        {
            GameManager.instance.Lose();
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer(spikeWallLayerName))
        {
            Debug.Log("ENTRO QUI");
            GameManager.instance.Lose();
        }
        else
        {
            Debug.Log("Collision with something else");
        }
    }
}
