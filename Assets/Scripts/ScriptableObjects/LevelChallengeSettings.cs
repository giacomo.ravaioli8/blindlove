﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Level Setting", menuName = "Level Setting")]
public class LevelChallengeSettings : ScriptableObject
{
    //Game
    public float timeAvailable;

    //Grid
    public float nodesLowerBound;
    public float nodesUpperBound;

    public float middleLowerBound;
    public float middleUpperBound;

    //Items and Game objects
    public int nGroundHoles;
    public float groundHolesVar;

    public float spikeInnerWallChance;
    
    public int nBombs;
    public int nDoubleThreat;
    public float bombsVar;

    public int nRewards;
    public float rewardsVar;

    public int timePup;
    public int lightPup;
    public float pUPVar;
}
