﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int highestRound;
    public int highestScore;

    public PlayerData()
    {
        highestRound = GameManager.instance.GetRoundNumber();
        highestScore = GameManager.instance.GetScore();
    }
}
