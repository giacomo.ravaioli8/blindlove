﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SavingSystem
{
    public static void Save(PlayerData playerData)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.high";

        FileStream stream = new FileStream(path, FileMode.Create);
        
        formatter.Serialize(stream, playerData);
        stream.Close();
    }

    public static PlayerData Load()
    {
        string path = Application.persistentDataPath + "/player.high";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();

            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData playerData = (PlayerData) formatter.Deserialize(stream);
            stream.Close();

            return playerData;
        }
        else
        {
            Debug.Log("Save file not found :(");
            return null;
        }

    }

    public static void Delete()
    {
        string path = Application.persistentDataPath + "/player.high";

        if (File.Exists(path))
            File.Delete(path);
    }
}
